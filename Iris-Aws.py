import pandas as pd
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder


df=pd.read_csv('iris.csv')

X=df.iloc[:,0:4]

y=df.iloc[:,4]

label=LabelEncoder()

y=label.fit_transform(y)

X_train,X_test,y_train,y_test=train_test_split(X,y,test_size=0.2,random_state=5)

modelo=KNeighborsClassifier()

modelo.fit(X_train,y_train)

pred=modelo.predict(X_test)

confusion_matrix(pred,y_test)

print(modelo.predict([[5,5.4,3.6,2.8]])[0])






